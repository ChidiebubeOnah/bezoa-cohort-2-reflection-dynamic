﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpandoObjectUse
{
    class Program
    {
        static void Main(string[] args)
        {

            //Declaration
            dynamic person = new ExpandoObject();

            person.Name = "Dynamic";
            person.Surname = "Person";
            person.Internship = "BEZAO";
            person.Health = "Sound";
            // etc.....

            foreach (var prop in person)
                Console.WriteLine($"{prop.Key} : {prop.Value}");


            // Another way

            var dyn = (IDictionary<string, object>)person;

            dyn.Add("Age", 23);
            dyn.Add("Gender", "Binary");

            Console.WriteLine("\n----- Updates------\n");
           
            //etc...

            foreach (var prop in person)
                Console.WriteLine($"{prop.Key} : {prop.Value}");



            Console.WriteLine("\n-----Adding Methods------\n");

            person.Check = (Func<bool>)(() => !string.IsNullOrWhiteSpace(person.Name) && !string.IsNullOrWhiteSpace(person.Surname));

            person.Write = (Action)(() =>
            {
                if (!person.Check()) return;

                foreach (var prop in person)
                    Console.WriteLine($"{prop.Key} : {prop.Value}");
            });


            person.Write();

            Console.WriteLine("\n-----Deleting Members------\n");
            dyn.Remove("Health");

            // dyn.Remove("Check"); what do you think will happen when you uncomment this?

            person.Write();


        }
    }
}
