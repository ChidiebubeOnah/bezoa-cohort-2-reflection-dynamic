﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Reflection_Dynamic
{
	class Program
	{
		static void Main(string[] args)
		{
			// Declare Instance of class Assembly
			// Call the GetExecutingAssembly method
			// to load the current assembly
			Assembly executing = Assembly.GetExecutingAssembly();
			var assemblyName = executing.FullName.Split(',').First();
			Console.WriteLine($"Info about {assemblyName}");
			// Array to store types of the assembly
			Type[] types = executing.GetTypes();
			foreach (var item in types)
			{
				// Display each type
				Console.WriteLine("Class : {0}", item.Name);

				// Array to store methods
				MethodInfo[] methods = item.GetMethods();
				foreach (var method in methods)
				{
					// Display each method
					Console.WriteLine("--> Method : {0}", method.Name);

					// Array to store parameters
					ParameterInfo[] parameters = method.GetParameters();
					foreach (var arg in parameters)
					{
						// Display each parameter
						Console.WriteLine("----> Parameter : {0} Type : {1}",
							arg.Name, arg.ParameterType);
					}
				}
			}
		}
	}




}
