﻿using System;

namespace Reflection_Dynamic
{
    class User
    {

        // Properties definition
        public int Id { get; set; }
        public string Name { get; set; }

      

        // No Argument Constructor
        public User()
        {
            Id = 0;
            Name = string.Empty;
        }

        // Parameterised Constructor
        public User(int id, string name)
        {
            Id = id;
            Name = name;
        }

        // Method to Display Student Data
        public void LogInfo()
        {
            Console.WriteLine($"ID : {Id}");
            Console.WriteLine($"Name : {Name}");
        }
    }
}